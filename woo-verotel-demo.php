<?php
/** 
 * Plugin Name: Mi plugin de pasarella de pagos de verotel
 * Description: Este plugin sirve para hacer determinada tarea
 * Version: 1.0.0
 * Author: Estratega.pe
 * Author URI: https://estratega.pe
 * Contributors: Jose Luis Huamani Gonzales
 * License: GPLv3
 * Text Domain: woo-verotel-demo
 * Domain Path: /languages
 * Requires at least: 5.0
 * Tested up to: 5.5
 * WC requires at least: 4.0.x
 * WC tested up to: 4.8
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if ( ! defined( 'ABSPATH' ) ) exit;

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option('active_plugins') ) ) ) {
	// Woocommerce esta activo
	define( 'WOO_VEROTEL_DEMO_VERSION', '1.0.0' );
	define( 'WOO_VEROTEL_DEMO_TET', 'tabla_demo_verotel' );
	define( 'WOOVD_PLUGIN_PATH', plugin_dir_path(__FILE__) );

	function woo_verotel_demo_install() {
		require_once plugin_dir_path( __FILE__ ) . 'classes/woo-verotel-demo-setup.php';
		Woo_Verotel_Demo_Setup::Activate();
	}
	function woo_verotel_demo_uninstall() {
		require_once plugin_dir_path( __FILE__ ) . 'classes/woo-verotel-demo-setup.php';
		Woo_Verotel_Demo_Setup::Deactivate();
	}

	register_activation_hook( __FILE__, 'woo_verotel_demo_install' );
	register_deactivation_hook( __FILE__, 'woo_verotel_demo_uninstall' );

	function woo_verotel_demo_run() {
		$plugin = new Woo_Verotel_Demo();
		$plugin->run();
	}

} else {
	function error_woocommerce_required() {
		$error_wc = wp_sprintf( __( '%sWoo verotel demo%s plugin requires %sWooCommerce%s activated. The plugin was deactivated until you active %sWooCommerce%s', 'woo-verotel-demo' ), '<strong>', '</strong>', '<strong>', '</strong>', '<strong>', '</strong>' );
		echo '
		<div class="notice notice-error is-dismissible">
			<p>' . $error_wc .'</p>
		</div>
		';

	}
	add_action( 'admin_notices', 'error_woocommerce_required' );
}