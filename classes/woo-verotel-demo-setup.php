<?php

class Woo_Verotel_Demo_Setup {
	public static function Activate() {
		self::CreateTables();
	}

	public static function Deactivate() {
		global $wpdb;

        $sql = "DROP TABLE IF EXISTS {$wpdb->prefix}" . WOO_VEROTEL_DEMO_TET;
        $wpdb->query($sql);
	}

	/**
	 * Private functions
	 */
	private static function CreateTables() {
		Self::Deactivate();

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();
		$sql = " CREATE TABLE {$wpdb->prefix}" . WOO_VEROTEL_DEMO_TET . " (
            id int unsigned NOT NULL AUTO_INCREMENT,
            name varchar(60) DEFAULT NULL,
            slug varchar(60) DEFAULT NULL,
            subject varchar(60) DEFAULT NULL,
            content text,
            PRIMARY KEY (`id`)
        ) " . $charset_collate . "; ";
		dbDelta( $sql );
	}
}
