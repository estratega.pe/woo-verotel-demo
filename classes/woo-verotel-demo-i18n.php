<?php

class Woo_Verotel_Demo_i18n {
    public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'woo-verotel-demo',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}
}