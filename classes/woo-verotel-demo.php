<?php

class Woo_Verotel_Demo {

	protected $loader;
	protected $plugin_name;
	protected $version;

	public function __construct() {
		if( defined( 'WOO_VEROTEL_DEMO_VERSION' ) ) {
			$this->version = WOO_VEROTEL_DEMO_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'woo-verotel-demo';
		$this->load_dependencies();
		$this->set_locate();
	}

	private function load_dependencies() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'classes/woo-verotel-demo-loader.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'classes/woo-verotel-demo-i18n.php';

		$this->loader = new Woo_Verotel_Demo_Loader();
	}

	private function set_locale() {
		$plugin_i18n = new Woo_Verotel_Demo_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * public functions
	 */
	public function run() {
		$this->loader->run();
	}

	public function get_loader() {
		return $this->loader;
	}

	public function get_plugin_name() {
		return $this->plugin_name;
	}

	public function get_version() {
		return $this->version;
	}
}